# Node Docker

> Este módulo mostra como montar um container docker com projeto em nodeJS

## Comandos

```bash
// montar o container
$ sudo docker-compose up
```

## Descrição

```yml
  version: '2' # versão da aplicação
  services:
    nodeApp:
      image: "node:8"
      container_name: nodeApp
      volumes:
        - .:/app # copia o projeto para pasta /app do container
      ports:
        - 3000:3000 # hostPort:appPort
      environment:
        - PORT=3000
      command: bash -c "cd /app; npm install; node index.js"
```