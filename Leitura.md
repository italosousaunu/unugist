# Leituras
Material de apoio. Artigos, livros, apostilas interessantes para aprendizado.

## Docker

* [Manual de Sobrevivência ao Docker](https://share.atelie.software/introdu%C3%A7%C3%A3o-a-baleia-azul-608208483015)
* [Subindo um Banco de Dados MySQL e phpMyAdmin com Docker](https://share.atelie.software/subindo-um-banco-de-dados-mysql-e-phpmyadmin-com-docker-642be41f7638)
* [Docker para o ambiente de desenvolvimento](https://phpzm.rocks/docker-para-o-ambiente-de-desenvolvimento-9bcba9a02288)
